  
  window.onload = function init() {

    document.getElementById("downloadLnk").addEventListener('click', saveFile, false);
    document.getElementById('file-input').addEventListener('change', loadFile, false);
    canvas = document.getElementById("paint");
    ctx = canvas.getContext('2d');
    pCanvas = document.getElementById("preview");
    pCtx = pCanvas.getContext('2d');
    
    input_layer_size = pCanvas.width * pCanvas.width;
    w1 = randInitWeights(input_layer_size,hidden_layer_size);
    w2 = randInitWeights(hidden_layer_size,10);

    var sketch = document.querySelector('#sketch');
    var sketch_style = getComputedStyle(sketch);

    var mouse = {x: 0, y: 0};
    var last_mouse = {x: 0, y: 0};

    /* Mouse Capturing Work */
    canvas.addEventListener('mousemove', function(e) {
        last_mouse.x = mouse.x;
        last_mouse.y = mouse.y;

        mouse.x = e.pageX - this.offsetLeft;
        mouse.y = e.pageY - this.offsetTop;
    }, false);

    /* Drawing on Paint App */
    ctx.lineWidth = 15;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'blue';

    canvas.addEventListener('mousedown', function(e) {
        canvas.addEventListener('mousemove', onPaint, false);
    }, false);

    window.addEventListener('mouseup', function() {
        canvas.removeEventListener('mousemove', onPaint, false);
    }, false);

    var onPaint = function() {
        ctx.beginPath();
        ctx.moveTo(last_mouse.x, last_mouse.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.closePath();
        ctx.stroke();
    };
  };

  function centreCanvas(imageData,width){
    var avgX = 0, avgY = 0;
    var count = 0;
    for(var x = 0; x < width; x++) {
      for(var y = 0; y < width; y++) {
        if(mapArray(x,y,imageData,width)>0){
          avgX+=x;
          avgY+=y;
          count++;
        }
      }
    }

    // get centre of the drawing
    avgX = Math.floor((width/2)-avgX/count);
    avgY = Math.floor((width/2)-avgY/count);
    // translate each of the "points" on the canvas
    var arr = new Array(width*width).fill(0);
    for(var x = 0; x < width; x++) {
      for(var y = 0; y < width; y++) {
        if(mapArray(x,y,imageData,width)>0) {
          var idx = x+avgX + width*(y+avgY);
          if(idx<=imageData.length && idx>0)
            arr[idx] = 255;
        }
      }
    }
    return arr;
  }

  // map 1d to 2d array
  function mapArray(x,y,array,width){
    return array[x + width*y];
  }

  function point(x, y){
    pCtx.strokeRect(x,y,3,3);
  }

  function loadTrainingX(val){
    var arr = [];
    for (i = 0; i < inputDataX[val].length; i++) {
        arr.push(0);
        arr.push(inputDataX[val][i]*255);
        arr.push(0);
        arr.push(255);
    }
    var imgData = new ImageData(new Uint8ClampedArray(arr), 40, 40);
    pCtx.putImageData(imgData, 0, 0);
  }


  function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  function canvasToArray(){
    pCtx.clearRect(0, 0, pCanvas.width, pCanvas.height);
    // draw to smaller canvas to scale down img
    pCtx.drawImage(canvas, 0, 0, pCanvas.width, pCanvas.height );
    imageData = pCtx.getImageData(0, 0, pCanvas.width, pCanvas.height);
    // put the blue channel into the training data
    var arr = [];
    for (i = 2; i < imageData.data.length; i+=4) {
      arr.push(imageData.data[i]);
    }
    var xy = centreCanvas(arr,pCanvas.width);
    inputDataX.push(xy);

    var y = document.getElementById("Y");
    yVal = parseInt(y.options[y.selectedIndex].value);

    // make sure y has a value
    if(yVal>=0){
      inputDataY.push(yVal);
      XSize++;
      
      var ele = document.getElementById("count"+yVal).innerHTML;
      document.getElementById("count"+yVal).innerHTML = parseInt(ele) + 1;
      document.getElementById("Training").innerHTML = XSize;
      var selectX = document.getElementById("viewX");
      var opt = document.createElement('option');

      opt.innerHTML = yVal;
      opt.value = XSize-1;
      selectX.appendChild(opt);
      loadTrainingX(XSize-1);
      clearCanvas();
    } else {
      alert("Invalid Input");
    }
  }