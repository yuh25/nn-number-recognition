function MSubInt(matrix, int){
    return matrix.map(function(x){return int-x;});
  }

  function IntSubM(matrix, int){
    return matrix.map(function(x){return x-int;});
  }

  function MAddInt(matrix, int){
    return matrix.map(function(x){return x+int;});
  }

  function MMultInt(matrix, int){
    return matrix.map(function(x){return x*int;});
  }

  function MDivInt(matrix, int){
    return matrix.map(function(x){return int/x;});
  }

  function IntDivM(matrix, int){
    return matrix.map(function(x){return x/int;});
  }

  function MLog(matrix){
    return matrix.map(function(x){return Math.log(x);});
  }

  function MdotSqr(matrix){
    return matrix.map(function(x){return x*x;});
  }

  function multElements(a,b){
    var output = [];
    if(a instanceof Matrix && b instanceof Matrix){
      for(var i = 0; i<a.dimensions().rows; i++){
        var row =  [];
        for(var j = 0; j<a.dimensions().cols; j++){
          row.push(a.elements[i][j] * b.elements[i][j]);
        }
        output.push(row);
      }
    }
    return $M(output);
  }

  function indexOfMax(arr) {
    if (arr.length === 0) {
        return -1;
    }

    var max = arr[0];
    var maxIndex = 0;

    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }
    return maxIndex;
  }

    function sigmoidGrad(z){
    if(z instanceof Matrix){
      var gz = z.map(function(x){
        var g = 1.0 / (Math.exp(-x) + 1.0);
        return g * (1.0 - g);
      });
      return gz;
    }
    return null;
  }


  function sigmoid(z){
    if(z instanceof Matrix){
      var g = z.map(function(x){return 1.0/(Math.exp(-x)+1.0);});
      return g;
    }
    return null;
  }

  function add(a,b){
    return a+b;
  }

  function sumMatrix(arr){
    if(arr instanceof Matrix){
      var temp = [];
      testb = arr;
      for(var i = 0; i< arr.elements.length-1;i++){
        var sum = arr.elements[i].reduce(add);
        temp.push(sum);
      }
      return Vector.create(temp);
    }
    return -1;
  }

  function ones(n) {
    var elements = [];
    while (n--) { elements.push(1); }
    return $M(elements);
  }
