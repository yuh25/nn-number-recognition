  var canvas,pCanvas,pCtx, ctx, inputDataX=[], inputDataY=[], XSize = 0;

  var w1, w2;

  var accuracy = 0;
  var iterNum = 1;
  
  var input_layer_size;
  var hidden_layer_size = 100;


  /*
  ---    ---  ---
   |      |    |
  6400 - 100 - 10
   |      |    |
  ---    ---  ---
  input  hidden    output
  */

  function randInitWeights(L_out,L_in){
    epsilon_init = 0.12;
    var temp = MMultInt(Matrix.Random(1 +L_out, L_in),2*epsilon_init);
    return IntSubM(temp,epsilon_init);
  }

  function forwardProp(X,y,k,theta1,theta2,lambda){
    m = XSize;

    a1 = X;
    z2 = gpuMatMult(X.elements,theta1.elements);
    if(!z2) console.log("theta1 wrong size");
    a2 = $M(sigmoidGPU(z2));

    a2 = ones(a2.dimensions().rows).augment(a2);
    a3 = sigmoid(a2.x(theta2));

    /*var correct = 0;
    for (var j = 0; j<a3.elements.length; j++) {
      if(indexOfMax(a3.elements[j]) == indexOfMax(y.elements[j]))
        correct++;
    }*/

    var Theta1 = theta1.dup();
    var Theta2 = theta2.dup();

    var x = theta1.elements.map(function(val){return val.slice(0, -1);});
    Theta1 = Matrix.Zero(x.length,1).augment($M(x));

    x = theta2.elements.map(function(val){return val.slice(0, -1);});
    Theta2 = Matrix.Zero(x.length,1).augment($M(x));

    //backprop
    d3 = a3.subtract(y);

    var t = w2.dup();
    t.elements.shift();
    d2 = multElements(d3.x(t.transpose()), $M(sigmoidGradGPU(z2)));
    Delta1 = d2.transpose().x(a1);
    Delta2 = d3.transpose().x(a2);

    Theta1_grad = IntDivM(Delta1,m);
    Theta2_grad = IntDivM(Delta2,m);

    Theta1_grad = Theta1_grad.add((MMultInt(Theta1, lambda / m)).transpose());
    Theta2_grad = Theta2_grad.add((MMultInt(Theta2, lambda / m)).transpose());
    
    return [0, Theta1_grad, Theta2_grad,correct/m*100.0];
  }


  function forwardPropGPU(X,y,k,theta1,theta2,lambda){
    m = XSize;
    //theta2 - 101 * 10;
    z2 = gpuMatMult(X.elements,theta1.elements);
    a2 = sigmoidGPU(z2);
    a3 = sigmoidGPU2(gpuMatMult2(a2, theta2.elements));

    var correct = 0;
    for (var j = 0; j<a3.length; j++) {
      if(indexOfMax(a3[j]) == indexOfMax(y.elements[j]))
        correct++;
    }

    //backprop
    d3 = $M(matSubtGPU(a3,y.elements)); // 401 * 10
    var tem2 = gpuMatMult3(d3.elements,theta2.elements);
    d2 = multElementsGPU(tem2, sigmoidGradGPU(z2)); // m x 100

    var Delta1 = gpuMatMult4(d2,X.elements);// 100x1601 (hidden*input) d2 - // m x 100

    Delta2 = d3.transpose().x(ones(m).augment($M(a2)));//add ones to a2[1 a2] 10x101 (output* hidden and bias)
    console.log(d3,a2,Delta2,gpuMatMult5(d3.elements,a2));

    Theta1_grad = IntDivM($M(Delta1),m);
    Theta2_grad = IntDivM(Delta2,m);

    var Theta1 = theta1.dup();// first col = 0;
    for (var i = 0; i < Theta1.elements.length; i++) {
      Theta1.elements[i][0] = 0;
    }
    a = MMultInt(Theta1, lambda / m);

    var Theta2 = theta2.dup();
    for (var i = 0; i < Theta2.elements.length; i++) {
      Theta2.elements[i][0] = 0;
    }
    b = MMultInt(Theta2, lambda / m);

    Theta1_grad = Theta1_grad.add(a.transpose());
    Theta2_grad = Theta2_grad.add(b.transpose());
    var lRate = 0.4;
    var temp1 = w1.subtract(MMultInt(Theta1_grad, lRate).transpose());
    var temp2 = w2.subtract(MMultInt(Theta2_grad, lRate).transpose());
    
    return [temp1, temp2, correct/m*100.0];
  }

  function train_NN(){
    if(XSize>0){
      var yEyeMat = Matrix.I(10);
      var y_matrix = [];
      for(var i = 0; i<XSize; i++){
        y_matrix.push(yEyeMat.elements[inputDataY[i]]);
      }
      var x = ones(XSize).augment($M(inputDataX));
      var trainNum = 0;
      var oldAcc = 0;
      var accuracy = 0;
      while(trainNum < 2){
        //var deltas = forwardProp(x,$M(y_matrix),10,w1,w2,1.00); // time
        var deltas = forwardPropGPU(x,$M(y_matrix),10,w1,w2,1.00);
        w1 = deltas[0];
        w2 = deltas[1];
        accuracy = deltas[2];
        console.log(iterNum,accuracy.toFixed(2));
        iterNum++;
        trainNum++;
      }
    }
  }

  function predictNum(){

    pCtx.clearRect(0, 0, pCanvas.width, pCanvas.height);
    // draw to smaller canvas to scale down img
    pCtx.drawImage(canvas, 0, 0, pCanvas.width, pCanvas.height );
    imageData = pCtx.getImageData(0, 0, pCanvas.width, pCanvas.height);
    // put the blue channel into the training data
    var arr = [];
    for (i = 2; i < imageData.data.length; i+=4) {
      arr.push(imageData.data[i]);
    }
    var xy = centreCanvas(arr,pCanvas.width);

    var output = predict(w1, w2, $M(xy).transpose());
    var h = indexOfMax(output.elements[0]);
    var accuracy = MMultInt(output,10).elements;
    var k = 0;
    var len = accuracy.length;
    var num = [];

    while(k < len){
      num.push(Math.round(accuracy[0][k]));
      k++;
    }

    var values = "";
    for (var i = 0; i < output.elements[0].length; i++) {
      values = values.concat(i + " " + (output.elements[0][i]*100).toFixed(2) + "%");
      if(i == h)
        values = values.concat(" <--");
      values = values.concat("<br>");
    }
    document.getElementById("values").innerHTML = values;
    document.getElementById("predict").innerHTML = h;
  }

  function predict(Theta1, Theta2, X){
    m = X.dimensions().rows;
    var h1 = sigmoid(ones(m).augment(X).x(Theta1));
    var h2 = sigmoid(ones(m).augment(h1).x(Theta2));
    if(m>1){
      var pOutput = [];
      for (var i = 0; i < m; i++) {
        pOutput.push(indexOfMax(h2.elements[i]));
      }
      return pOutput;
    }
    return h2;
  }

  function transposet(x){
    var rows = x.length, cols = x[0].length, j;
    var elements = [], i = cols;
    while (i--) { j = rows;
      elements[i] = [];
      while (j--) {
        elements[i][j] = x[j][i];
      }
    }
    return elements;
  }

  function viewWeights(){
    var temp = w1.elements;
    var neuron = Math.floor(Math.random()*hidden_layer_size);
    var scale = -Math.min.apply(Math,temp[neuron]);
    var arr = [];
    for (i = 1; i < 1601; i++) {
        arr.push(0);
        arr.push((temp[i][neuron]+scale)*255);
        arr.push(0);
        arr.push(255);
    }
    var imgData = new ImageData(new Uint8ClampedArray(arr), 40, 40);
    pCtx.putImageData(imgData, 0, 0);
  }

  const gpu = new GPU();

  const myFunc = gpu.createKernel(function(a,b) {
    return a+b;
  }).setOutput([100]);

  const gpuMatMult = gpu.createKernel(function (A, B) {
    var sum = 0;
    for (var i = 0; i < 1601; i++) { // a -X 1601 * 401 b - w1 1601 * 100
        sum += A[this.thread.y][i] * B[i][this.thread.x];
    }
    return sum;
  })
  .setOutput([101, 401]);

  const gpuMatMult2 = gpu.createKernel(function (A, B) {
    var sum = 0;
    sum += B[0][this.thread.x];
    for (var i = 1; i < 101; i++) {
        sum += A[this.thread.y][i] * B[i+1][this.thread.x];
    }
    return sum;
  })
  .setOutput([10, 401]);

  const gpuMatMult3 = gpu.createKernel(function (A, B) {
    var sum = 0;
    for (var i = 1; i < 10; i++) {
        sum += A[this.thread.y][i] * B[this.thread.x+1][i];
    }
    return sum;
  })
  .setOutput([100, 401]);


  const gpuMatMult4 = gpu.createKernel(function (A, B) {
    var sum = 0;
    for (var i = 0; i < 401; i++) {
        sum += A[i][this.thread.y] * B[i][this.thread.x];
    }
    return sum;
  })
  .setOutput([1601, 100]);


  const gpuMatMult5 = gpu.createKernel(function (A, B) {
    var sum = 0;
    sum += A[0][this.thread.y];
    for (var i = 1; i < 401; i++) {
        sum += A[i][this.thread.y] * B[i+1][this.thread.x];
    }
    return sum;
  })
  .setOutput([101, 10]);
//d3 - 401 * 10
//Delta2 = d3.transpose().x(ones(m).augment(a2));//add ones to a2[1 a2] 10x101 (output* hidden and bias)

  const sigmoidGPU = gpu.createKernel(function (z) {
    return 1.0/(exp(-z[this.thread.y][this.thread.x])+1.0);
  }).setOutput([100, 401]);

  const sigmoidGPU2 = gpu.createKernel(function (z) {
    return 1.0/(exp(-z[this.thread.y][this.thread.x])+1.0);
  }).setOutput([10, 401]);

  const sigmoidGradGPU = gpu.createKernel(function (z) {
    var g = (1.0 / (Math.exp(-z[this.thread.y][this.thread.x]) + 1.0));
    return g * (1.0 - g);
  }).setOutput([100, 401]);

  const matSubtGPU = gpu.createKernel(function (a,b) {
    return a[this.thread.y][this.thread.x] - b[this.thread.y][this.thread.x];
  }).setOutput([10, 401]);


  const multElementsGPU = gpu.createKernel(function (a,b) {
    return b[this.thread.y][this.thread.x] * a[this.thread.y][this.thread.x];
  }).setOutput([100, 401]);
/*  
.setOutputToTexture(true)
,

    var elements = [];
    while (n--) { elements.push(1); }
    return $M(elements);

  
/* ¯\_(ツ)_/¯
const gpuMatMult = gpu.createKernel(function (A, B) {
        var sum = 0;
        for (var i = 0; i < 1600; i++) {
            sum += A[this.thread.y][i] * B[i][this.thread.x];
        }
        return sum;
})
.setOutput([401, 100]);

//

const megaKernel = gpu.createKernelMap([
  function add(a, b) {
    return a + b;
  },
  function multiply(a, b) {
    return a * b;
  }
], function(a, b, c) {
  return multiply(add(a[this.thread.x], b[this.thread.x]), c[this.thread.x]);
});

*/
